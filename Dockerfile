# our base image
FROM nginx:latest

LABEL AUTHOR=sidhik

COPY index.html /usr/share/nginx/html/index.html
COPY resources /usr/share/nginx/html/resources 